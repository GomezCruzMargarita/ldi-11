/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package LDI_11;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.TreeMap;

/**
 *
 * @author Margarita
 */
public class Operaciones {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Scanner entrada = new Scanner(System.in);
        Map <String, TipoDato> valores = new TreeMap<>();
        int op;
        
        String clave1;
        String clave2;
        String tipo;
        String dato;
        
        valores.put("1", new TipoDato("1","Byte","10101010"));
        valores.put("2", new TipoDato("2","Word","1010101011111111"));
        valores.put("3", new TipoDato("3","DWord","10101010110011001111100111100111"));
        valores.put("4", new TipoDato("4","Qword","1010101011001100111110011110011110101010110011001111100111100111"));
        valores.put("5", new TipoDato("5","Byte","10101111"));
       
      do{
       System.out.println(" ");
       System.out.println("1.- Ver Tabla de Valores");
       System.out.println("2.- Guardar un Valor");
       System.out.println("3.- Realizar Operacion con Registros");
       System.out.println("4.- Salir");
       op=entrada.nextInt();
       
       switch(op) 
       {
           case 1:
                System.out.println(" ");
                System.out.println("********* Tabla de Datos Guardados *********");
                valores.entrySet().stream().map((entradas) -> entradas.getValue()).forEach((valor) -> {
                System.out.println(valor.toString());
                 });
               break;
           case 2:
               System.out.println(" ");
               System.out.println("***** Ingresa los siguientes datos para guardar un valor *****");
               System.out.println("Ingresa la clave unica: ");
               clave1=entrada.next();
               System.out.println("Confirma la clave unica: ");
               clave2=entrada.next();
               System.out.println("Ingresa el tipo de dato: ");
               tipo=entrada.next();
               System.out.println("Ingresa el valor: ");
               dato=entrada.next();
               
               if(valores.containsKey(clave1) == false && valores.containsKey(clave2) == false)
               {    
                   if(clave1.equals(clave2)){
                       valores.put(clave1, new TipoDato(clave2,tipo,dato));
                       System.out.println("Valor agregado con exito");
                   }
                   else
                   {
                       System.err.println("Las claves no coinciden, verifique sus datos");
                   }
               }
               
               else
               {
                   System.err.println("Esta clave ya se encuentra en la tabla, verifique sus datos");
               }
               break;
           case 3:
            int opReg;
            String id;
            
            do{
            System.out.println(" ");
            System.out.println(" *** Operaciones con registros *** ");
            System.out.println("Seleciona el registro al cual quieres mover el dato");
            System.out.println("1.- Registro AX");
            System.out.println("2.- Registro AH");
            System.out.println("3.- Registro AL");
            System.out.println("4.- Registro BX");
            System.out.println("5.- Registro BH");
            System.out.println("6.- Registro BL");
            System.out.println("7.- Registro CX");
            System.out.println("8.- Registro CH");
            System.out.println("9.- Registro CL");
            System.out.println("10.- Registro DX");
            System.out.println("11.- Registro DH");
            System.out.println("12.- Registro DL");
            System.out.println("0.- Salir");
            opReg=entrada.nextInt();
       
       switch(opReg) 
       {
           
      case 1:
          System.out.println("Registro AX");
          System.out.println("Ingresa el id del valor guardado para realizar la operacion");
          id= entrada.next();
           
          if(valores.get(id).getValor().length() == 8 || valores.get(id).getValor().length() == 16)
          {
              System.out.println("El valor " + valores.get(id).getValor() + " fue movido con exito al registro AX");
          }
          else
          {
              System.err.println("El valor ingresado no es compatible con el registro AX, solo admite 16 bits");
          }
        break;
               
      case 2:
         
          System.out.println("Registro AH");
          System.out.println("Ingresa el id del valor guardado para realizar la operacion");
          id= entrada.next();
           
          if(valores.get(id).getValor().length() == 8 )
          {
              System.out.println("El valor " + valores.get(id).getValor() + " fue movido con exito al registro AH");
          }
          else
          {
              System.err.println("El valor ingresado no es compatible con el registro AH, solo admite 8 bits");
          }
        break;
       
      case 3:
          
          System.out.println("Registro AL");
          System.out.println("Ingresa el id del valor guardado para realizar la operacion");
          id= entrada.next();
           
          if(valores.get(id).getValor().length() == 8 )
          {
              System.out.println("El valor " + valores.get(id).getValor() + " fue movido con exito al registro AL");
          }
          else
          {
              System.err.println("El valor ingresado no es compatible con el registro AL, solo admite 8 bits");
          }
        break;
      
      case 4:
       
          System.out.println("Registro BX");
          System.out.println("Ingresa el id del valor guardado para realizar la operacion");
          id= entrada.next();
           
          if(valores.get(id).getValor().length() == 8 || valores.get(id).getValor().length() == 16 ||
            valores.get(id).getValor().length() == 32 || valores.get(id).getValor().length() == 64 )
          {
              System.out.println("El valor " + valores.get(id).getValor() + " fue movido con exito al registro BX");
          }
          else
          {
              System.err.println("El valor ingresado no es compatible con el registro BX, solo admite 16 bits");
          }
        break;
          
      case 5:
          
          System.out.println("Registro BH");
          System.out.println("Ingresa el id del valor guardado para realizar la operacion");
          id= entrada.next();
           
          if(valores.get(id).getValor().length() == 8 )
          {
              System.out.println("El valor " + valores.get(id).getValor() + " fue movido con exito al registro BH");
          }
          else
          {
              System.err.println("El valor ingresado no es compatible con el registro BH, solo admite 8 bits");
          }
        break;
      
       case 6:
            System.out.println("Registro BL");
          System.out.println("Ingresa el id del valor guardado para realizar la operacion");
          id= entrada.next();
           
          if(valores.get(id).getValor().length() == 8 )
          {
              System.out.println("El valor " + valores.get(id).getValor() + " fue movido con exito al registro BL");
          }
          else
          {
              System.err.println("El valor ingresado no es compatible con el registro BL, solo admite 8 bits");
          }
       break;
      
       case 7:
           
          System.out.println("Registro CX");
          System.out.println("Ingresa el id del valor guardado para realizar la operacion");
          id= entrada.next();
           
          if(valores.get(id).getValor().length() == 8 || valores.get(id).getValor().length() == 16)
          {
              System.out.println("El valor " + valores.get(id).getValor() + " fue movido con exito al registro CX");
          }
          else
          {
              System.err.println("El valor ingresado no es compatible con el registro CX, solo admite 16 bits");
          }
       break;
      
       case 8:
            System.out.println("Registro CH");
          System.out.println("Ingresa el id del valor guardado para realizar la operacion");
          id= entrada.next();
           
          if(valores.get(id).getValor().length() == 8 )
          {
              System.out.println("El valor " + valores.get(id).getValor() + " fue movido con exito al registro CH");
          }
          else
          {
              System.err.println("El valor ingresado no es compatible con el registro CH, solo admite 8 bits");
          }
        break;
           
       case 9:
            System.out.println("Registro CL");
          System.out.println("Ingresa el id del valor guardado para realizar la operacion");
          id= entrada.next();
           
          if(valores.get(id).getValor().length() == 8 )
          {
              System.out.println("El valor " + valores.get(id).getValor() + " fue movido con exito al registro CL");
          }
          else
          {
              System.err.println("El valor ingresado no es compatible con el registro CL, solo admite 8 bits");
          }
        break;
       
       case 10:
           
          System.out.println("Registro DX");
          System.out.println("Ingresa el id del valor guardado para realizar la operacion");
          id= entrada.next();
           
          if(valores.get(id).getValor().length() == 8 || valores.get(id).getValor().length() == 16 )
          {
              System.out.println("El valor " + valores.get(id).getValor() + " fue movido con exito al registro DX");
          }
          else
          {
              System.err.println("El valor ingresado no es compatible con el registro DX, solo admite 16 bits");
          }
        break; 
      
       case 11:
           
            System.out.println("Registro DH");
          System.out.println("Ingresa el id del valor guardado para realizar la operacion");
          id= entrada.next();
           
          if(valores.get(id).getValor().length() == 8 )
          {
              System.out.println("El valor " + valores.get(id).getValor() + " fue movido con exito al registro DH");
          }
          else
          {
              System.err.println("El valor ingresado no es compatible con el registro DH, solo admite 8 bits");
          }
        break; 
      
        case 12:
            
            System.out.println("Registro DL");
          System.out.println("Ingresa el id del valor guardado para realizar la operacion");
          id= entrada.next();
           
          if(valores.get(id).getValor().length() == 8 )
          {
              System.out.println("El valor " + valores.get(id).getValor() + " fue movido con exito al registro DL");
          }
          else
          {
              System.err.println("El valor ingresado no es compatible con el registro DL, solo admite 8 bits");
          }
        break; 
       
       }
      }while(opReg != 0);
       
               
       }
      }while(op != 4);
        
    }
    
}
