/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package LDI_11;

/**
 *
 * @author Margarita
 */
public class TipoDato {
    private String llave;
    private String tipo;
    private String valor;
    

    public TipoDato() {
    }

    public TipoDato(String llave, String tipo, String valor) {
        this.llave = llave;
        this.tipo = tipo;
        this.valor = valor;
    }
    public String getLlave() {
        return llave;
    }

    public void setLlave(String llave) {
        this.llave = llave;
    }
 
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    @Override
    public String toString() {
        return "Clave unica: "+this.llave+ " ---- " +"Tipo: "+this.tipo +" ---- "+" Valor: "+this.valor;
    }
    
    
}
